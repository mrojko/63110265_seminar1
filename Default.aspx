﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63110265_Seminar1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="CSSstyle1.css" type="text/css" title="default" id="stylesheet" />
</head> 
<body>



    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="L_DodatneZahteve" runat="server" style="text-align: right; float: right" Text="F1, F3, F4"></asp:Label>
        <asp:DropDownList ID="DL_CSS" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DL_ChangeCSS_SelectedIndexChanged" CssClass="SelectedRowStyle">
            <asp:ListItem Value="default">CSS stil 1</asp:ListItem>
            <asp:ListItem Value="alternate">CSS stil 2</asp:ListItem>
        </asp:DropDownList>




        <br />
    
        <hr />
        <br />
        <asp:Label ID="L_OddelkiInSkupine" runat="server" Text="Oddelki in skupine izdelkov" Font-Bold="True" Font-Size="X-Large" CssClass="Labels"></asp:Label>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT ProductCategoryID, Name FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)"></asp:SqlDataSource>
        <asp:DropDownList ID="DDL_Categories" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="ProductCategoryID" CssClass="SelectedRowStyle" OnSelectedIndexChanged="DDL_Categories_SelectedIndexChanged">
        </asp:DropDownList>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" GridLines="None" SelectedIndex="0" ShowHeader="False" DataKeyNames="ProductCategoryID" OnSelectedIndexChanged="GV_Categories_SelectedIndexChanged" CellPadding="5">
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" HeaderText="Name" Visible="False">
                </asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="ProductCategoryID" InsertVisible="False" ReadOnly="True" SortExpression="ProductCategoryID" />
                <asp:CommandField ButtonType="Button" SelectText="&gt;" ShowSelectButton="True" />
            </Columns>
            <SelectedRowStyle CssClass="SelectedRowStyle" />
        </asp:GridView>
        <br />
        <hr />
        <br />
        <asp:Label ID="L_Modeli" runat="server" Text="Modeli" Font-Bold="True" Font-Size="X-Large" CssClass="Labels"></asp:Label>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModel.Name, SalesLT.ProductDescription.Description, SalesLT.Product.ProductModelID FROM SalesLT.ProductModel INNER JOIN SalesLT.Product ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.Product.ProductCategoryID = @filter) AND (SalesLT.ProductModelProductDescription.Culture = @filter1)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" DefaultValue="@filter" Name="filter" PropertyName="SelectedValue" Type="Object" />
                <asp:Parameter DefaultValue="en" Name="filter1" Type="Object" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GV_Models" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ProductModelID" DataSourceID="SqlDataSource3" GridLines="None" SelectedIndex="0" ShowHeader="False" OnSelectedIndexChanged="GV_Models_SelectedIndexChanged" PageSize="5" CellPadding="5" CssClass="selectedRowStyle">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" ShowHeader="False" SortExpression="Name" />
                <asp:BoundField DataField="Description" HeaderText="Description" ShowHeader="False" SortExpression="Description" />
                <asp:CommandField SelectText="Prikaži izdelke" ShowSelectButton="True" />
            </Columns>
            <SelectedRowStyle CssClass="SelectedRowStyle" />
        </asp:GridView>
        <br />
        <hr />
        <br />
        <asp:Label ID="L_Izdelki" runat="server" Text="Izdelki" Font-Bold="True" Font-Size="X-Large" CssClass="Labels"></asp:Label>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT ThumbNailPhoto, ProductNumber, Name, Size, Color, ListPrice, ThumbnailPhotoFileName FROM SalesLT.Product WHERE (ProductCategoryID = @filter) AND (ProductModelID = @filter1)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" DefaultValue="@filter" Name="filter" PropertyName="SelectedValue" Type="Object" />
                <asp:ControlParameter ControlID="GV_Models" DefaultValue="" Name="filter1" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GV_Products" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductNumber" DataSourceID="SqlDataSource4" GridLines="None" OnSelectedIndexChanged="GV_Products_SelectedIndexChanged" CellPadding="5" OnRowDataBound="GV_Products_RowDataBound">
            <Columns>
                <asp:ImageField DataImageUrlField="ThumbnailPhotoFileName" DataImageUrlFormatString="~/ShowImage.ashx?ID={0}">
                </asp:ImageField>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Cena" SortExpression="ListPrice" />
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
                <asp:BoundField DataField="ThumbnailPhotoFileName" SortExpression="ThumbnailPhotoFileName" Visible="False" />
            </Columns>
            <SelectedRowStyle CssClass="SelectedRowStyle" />
        </asp:GridView>
        <br />
        <asp:Label ID="L_Product" runat="server" Text="Podatki o izdelku" Font-Bold="True" Font-Size="Large" Visible="False" CssClass="Labels"></asp:Label>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, Weight, StandardCost, ListPrice FROM SalesLT.Product WHERE (ProductNumber = @filter)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" DefaultValue="filter" Name="filter" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" CellSpacing="5" DataSourceID="SqlDataSource5" GridLines="None" Height="50px" Width="225px">
            <Fields>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Weight" HeaderText="Teža" SortExpression="Weight" />
                <asp:BoundField DataField="StandardCost" HeaderText="Nabavna cena" SortExpression="StandardCost" DataFormatString="{0:c}" />
                <asp:BoundField DataField="ListPrice" HeaderText="Prodajna cena" SortExpression="ListPrice" DataFormatString="{0:c}" />
            </Fields>
        </asp:DetailsView>
        <br />
        <asp:Label ID="L_Orders" runat="server" Text="Seznam naročil" Font-Bold="True" Font-Size="Large" Visible="False" CssClass="Labels"></asp:Label>
        <br />
        <asp:TextBox ID="TB_FilterOrders" runat="server" AutoPostBack="True" Visible="False" OnTextChanged="TB_FilterOrders_TextChanged"></asp:TextBox>
        <br />
        <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:AdventureWorksLT2012_DataConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.SalesOrderHeader.OrderDate, SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ', ' + SalesLT.Address.CountryRegion + ')' AS CompanyName, SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID INNER JOIN SalesLT.Product ON SalesLT.SalesOrderDetail.ProductID = SalesLT.Product.ProductID WHERE (SalesLT.Product.ProductNumber = @filter) AND (SalesLT.Customer.CompanyName + ' (' + SalesLT.Address.City + ', ' + SalesLT.Address.CountryRegion + ')' LIKE '%'+@filter2+'%')">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" DefaultValue="@filter" Name="filter" PropertyName="SelectedValue" Type="Object" />
                <asp:ControlParameter ControlID="TB_FilterOrders" DefaultValue="%" Name="filter2" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:GridView ID="GV_Orders" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellSpacing="5" DataSourceID="SqlDataSource6" GridLines="None" OnDataBound="GV_Orders_DataBound" OnLoad="GV_Orders_Load">
            <Columns>
                <asp:BoundField DataField="SalesOrderID" SortExpression="SalesOrderID" />
                <asp:BoundField DataField="OrderDate" HeaderText="Datum" SortExpression="OrderDate" DataFormatString="{0:d}" />
                <asp:BoundField DataField="CompanyName" HeaderText="Kupec" ReadOnly="True" SortExpression="CompanyName" />
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" />
                <asp:BoundField DataField="UnitPrice" DataFormatString="{0:c}" HeaderText="Cena enote" SortExpression="UnitPrice" />
                <asp:BoundField DataField="UnitPriceDiscount" HeaderText="Popust" SortExpression="UnitPriceDiscount" DataFormatString="{0:p}" />
                <asp:BoundField DataField="LineTotal" DataFormatString="{0:c}" HeaderText="Cena" ReadOnly="True" SortExpression="LineTotal" />
            </Columns>
        </asp:GridView>
    
        </div>
    </form>
</body>
</html>
