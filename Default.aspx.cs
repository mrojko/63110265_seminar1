﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Reflection;
using System.Drawing;
using System.Web.UI;

namespace _63110265_Seminar1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) {
            }
        }


        protected void GV_Products_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GV_Products.SelectedIndex != -1) {
                L_Product.Visible = true;
            }
            L_Orders.Visible = false;
            TB_FilterOrders.Visible = false;
            TB_FilterOrders.Text = "";
        }

        protected void GV_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            L_Orders.Visible = false;
            TB_FilterOrders.Visible = false;
            TB_FilterOrders.Text = "";
        }

        protected void GV_Models_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            L_Orders.Visible = false;
            TB_FilterOrders.Visible = false;
            TB_FilterOrders.Text = "";
        }

        protected void GV_Orders_Load(object sender, EventArgs e)
        {
        }

        protected void GV_Orders_DataBound(object sender, EventArgs e)
        {
            if (GV_Orders.Rows.Count != 0)
            {
                L_Orders.Visible = true;
                TB_FilterOrders.Visible = true;
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void DL_ChangeCSS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DL_CSS.SelectedItem.Value.ToString() == "alternate")
                stylesheet.Href = "CSSstyle2.css";
            else
                stylesheet.Href = "CSSstyle1.css";
        }

        public Image byteArrayToImage(byte[] byteBLOBData)
        {
            MemoryStream ms = new MemoryStream(byteBLOBData);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        protected void GV_Products_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {

        }

        protected void TB_FilterOrders_TextChanged(object sender, EventArgs e)
        {
        }

        protected void DDL_Categories_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Categories.SelectedIndex = 0;
            GV_Models.SelectedIndex = 0;
            GV_Products.SelectedIndex = -1;
            L_Product.Visible = false;
            L_Orders.Visible = false;
            TB_FilterOrders.Visible = false;
            TB_FilterOrders.Text = "";
        }
    }
}