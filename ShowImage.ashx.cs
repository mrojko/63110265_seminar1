﻿
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System;

namespace _63110265_Seminar1
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string my_Id;
            if (context.Request.QueryString["ID"] != null)
            {
                my_Id = (context.Request.QueryString["ID"]);
                context.Response.ContentType = "image/jpeg";
                Stream strm = ShowEmpImage(my_Id);
                byte[] buffer = new byte[4096];
                int byteSeq = strm.Read(buffer, 0, 4096);
                while (byteSeq > 0)
                {
                    context.Response.OutputStream.Write(buffer, 0, byteSeq);
                    byteSeq = strm.Read(buffer, 0, 4096);
                }
            }
        }

        public Stream ShowEmpImage(string my_Id)
        {
            string conn = ConfigurationManager.ConnectionStrings["AdventureWorksLT2012_DataConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(conn);
            string sql = "select ThumbnailPhoto from SalesLT.Product WHERE ThumbnailPhotoFileName = @ID";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", my_Id);
            connection.Open();
            object img = cmd.ExecuteScalar();
            return new MemoryStream((byte[])img);
        }

        bool IHttpHandler.IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}